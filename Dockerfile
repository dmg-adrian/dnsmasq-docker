FROM ubuntu:16.04
RUN apt-get update && apt-get -y install dnsmasq
EXPOSE 53 53/udp
ENTRYPOINT ["dnsmasq", "-k"]
